#include "Algo.h"

bool Algo::predict(const std::string &s) {
    w = s;

    dynamic_holder.clear();
    dynamic_holder.resize(w.size() + 1);

    dynamic_holder[0].insert(Case(g.getRules()[0], 0, 0));
    size_t current_size;

    do {
        current_size = dynamic_holder[0].size();
        forecast(0);
        complete(0);
    } while (dynamic_holder[0].size() != current_size);

    for (size_t i = 1; i <= w.size(); ++i) {
        scan(i - 1, w[i - 1]);
        do {
            current_size = dynamic_holder[i].size();
            forecast(i);
            complete(i);
        } while (dynamic_holder[i].size() != current_size);
    }

    return dynamic_holder[w.size()].find(Case(g.getRules()[0], 1, 0)) != dynamic_holder[w.size()].end();
}

void Algo::forecast(size_t j) {
    std::vector<Case> new_situations;
    for (std::set<Case>::iterator case_it = dynamic_holder.at(j).begin();
         case_it != dynamic_holder.at(j).end();
         ++case_it
            ) {
        std::vector<Transition> new_rules = g.getRules();
        for (std::vector<Transition>::const_iterator rule = new_rules.begin();
             rule != new_rules.end();
             ++rule) {
            if (rule->first == case_it->trans.second[case_it->point]) {
                new_situations.push_back(Case(*rule, 0, j));
            }
        }
    }

    for (std::vector<Case>::const_iterator situation = new_situations.begin();
         situation != new_situations.end();
         situation++) {
        dynamic_holder[j].insert(*situation);
    }
}

void Algo::scan(size_t j, char x) {
    for (std::set<Case>::iterator situation = dynamic_holder[j].begin();
         situation != dynamic_holder[j].end();
         ++situation) {
        if (situation->trans.second[situation->point] == x) {
            dynamic_holder[j + 1].insert(Case(situation->trans, situation->point + 1, situation->i));
        }
    }
}

void Algo::complete(size_t j) {
    std::vector<Case> new_situations;
    for (std::set<Case>::iterator situation_a = dynamic_holder[j].begin();
         situation_a != dynamic_holder[j].end();
         ++situation_a) {
        if (situation_a->point != situation_a->trans.second.size()) continue;
        for (std::set<Case>::iterator situation_b = dynamic_holder[situation_a->i].begin();
             situation_b != dynamic_holder[situation_a->i].end();
             situation_b++){
            if (situation_b->trans.second[situation_b->point] == situation_a->trans.first) {
                new_situations.push_back(Case(situation_b->trans, situation_b->point + 1, situation_b->i));
            }
        }
    }

    for (std::vector<Case>::iterator situation = new_situations.begin();
         situation != new_situations.end();
         situation++) {
        dynamic_holder[j].insert(*situation);
    }
}