#pragma once
#include "includes.h"

struct Case {
    Transition trans;
    size_t point;
    size_t i;

    Case(Transition rule, size_t point, size_t i) : trans(rule), point(point), i(i) {};

    bool operator<(const Case& another) const {
        if (trans == another.trans) {
            if (point == another.point) {
                return i < another.i;
            }
            return point < another.point;
        }
        return trans < another.trans;
    }
};