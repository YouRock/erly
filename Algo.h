#pragma once
#include "includes.h"
#include "Grammar.h"
#include "Case.h"

class Algo {
public:
    Algo(const Grammar &_g) : g(_g) {}

    bool predict(const std::string &s);
private:
    void forecast(size_t j);
    void scan(size_t j, char x);
    void complete(size_t j);

    std::string w;
    std::vector<std::set<Case>> dynamic_holder;
    Grammar g;
};