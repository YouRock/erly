#include "includes.h"
#include "Algo.h"

void verify(Algo &dp_pars, const std::string &s, bool good) {
    std::cout << "Verifying: " << s << "\n";
    if (good) {
        assert(dp_pars.predict(s));
        std::cout << "good sample successfully verified\n";
    } else {
        assert(dp_pars.predict(s) != 1);
        std::cout << "bad sample successfully verified\n";
    }
}
int main() {
    std::vector<char> N = {'U', 'S', 'C', 'D'};
    std::vector<char> sigma = {'a', 'b', 'c'};
    std::vector<Transition> P = {
            {'U', "S"},
            {'S', "C"},
            {'D', "aDb"},
            {'S', "SC"},
            {'C', "cD"},
            {'D', ""},
    };
    char S = 'U';

    Grammar grammar = Grammar(N, sigma, P, S);
    Algo dp_pars(grammar);
    std::vector<std::string> good_samples = {"c", "cc", "ccc", "cab", "ccab", "cabc", "caabb", "cabcab", "ccaabb", "caabbc", "caaabbb", "ccaaabbb", "cabcaabb", "caabbcab", "caaabbbc", "caaaabbbb", "ccaaaabbbb", "caaaaabbbbb", "caaaaaabbbbbb", "caaaaaaabbbbbbb"};

    for (auto test: good_samples) {
        verify(dp_pars, test, 1);
    }

    std::vector<std::string> bad_samples = {"babcabababc", "babcabababcc", "babcabababccc", "babcabababcab", "babcabababccab", "babcabababcabc", "babcabababcaabb", "babcabababcabcab", "babcabababccaabb", "babcabababcaabbc", "babcabababcaaabbb", "babcabababccaaabbb", "babcabababcabcaabb", "babcabababcaabbcab", "babcabababcaaabbbc", "babcabababcaaaabbbb", "babcabababccaaaabbbb", "babcabababcaaaaabbbbb", "babcabababcaaaaaabbbbbb", "babcabababcaaaaaaabbbbbbb"};

    for (auto test: bad_samples) {
        verify(dp_pars, test, 0);
    }
    return 0;
}