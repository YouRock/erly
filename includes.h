#pragma once
#include <iterator>
#include <vector>
#include <iostream>
#include <set>
#include <cassert>

using Transition = std::pair<char, std::string>;