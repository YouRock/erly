#pragma once
#include "includes.h"

class Grammar {
private:
    std::vector<char> non_terminals;
    std::vector<char> terminals;
    std::vector<Transition> transitions;
    char S;
public:
    Grammar(std::vector<char> non_terminals,
            std::vector<char> terminals,
            std::vector<Transition> transitions,
            char S) : non_terminals(non_terminals), terminals(terminals), transitions(transitions), S(S) {}

    const std::vector<Transition>& getRules() const {
        return transitions;
    }
};